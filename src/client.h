/**
***********************************************
***            C++ Shared Memory            ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***               MIT License               ***
***********************************************
*/


/**
 * Shared-Memory Inter-Process-Communication
 * Client - Accesses the memory
 */
#ifndef SHMEM_CLIENT_H_
#define SHMEM_CLIENT_H_


#include <cstddef> // size_t
#include <cstdint> // uint8_t

#include <stdexcept> // C++ standard exceptions

#include <boost/interprocess/mapped_region.hpp>
#include <boost/interprocess/shared_memory_object.hpp>
#include <boost/interprocess/sync/interprocess_mutex.hpp>


namespace shmem {

class Client {
	public:
		/**
		 * Constructor
		 * @param name - ID of memory (for Boost library)
		 */
		Client(const char* name){
			// check arguments
			if(name == nullptr){
				throw ::std::invalid_argument("Null-pointer as shared memory ID given!");
			};
			
			// simplify calls
			using namespace ::boost::interprocess;
			
			// open memory
			this->shm = new shared_memory_object(open_only, name, read_write);// read-write always needed for the mutex
			
			// map the whole shared memory to this process
			try {
				this->region = new mapped_region(*(this->shm), read_write);// read-write always needed for the mutex
				
				// init mutex
				void* addr = this->region->get_address();
				this->mtx = static_cast<interprocess_mutex*>(addr);
			}
			catch(...){// catch any exception
				// free shared memory
				delete this->shm;
				
				// re-throw catched exception
				throw;
			};
		} // constructor
		
		/**
		 * Destructor
		 */
		~Client(){
			// free region
			delete this->region;
			
			// free shared memory
			delete this->shm;
		} // destructor
		
		
		/**
		 * Returns base adress of shared memory
		 * @return pointer to base address
		 */
		inline void* getAddress(){
			return (void*) ((::std::uint8_t*) this->region->get_address() + sizeof(*(this->mtx)));
		} // getAddress
		
		/**
		 * Returns size of shared memory
		 * @return size
		 */
		inline ::std::size_t getSize(){
			return this->region->get_size() - sizeof(*(this->mtx));
		} // getSize
		
		/**
		 * Returns associated IPC mutex
		 * @return mutex
		 */
		inline ::boost::interprocess::interprocess_mutex& getMutex(){
			return *(this->mtx);
		} // getMutex
	
	
	private:
		/** ID of memory (for Boost library) */
		char* name;
		
		/** Pointer to Boost shared memory */
		::boost::interprocess::shared_memory_object* shm;
		
		/** Pointer to Boost mapped region of shared memory */
		::boost::interprocess::mapped_region* region;
		
		/** Pointer to Boost mutex */
		::boost::interprocess::interprocess_mutex* mtx;
}; // class Client

} // namespace shmem


#endif
