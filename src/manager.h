/**
***********************************************
***            C++ Shared Memory            ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***               MIT License               ***
***********************************************
*/


/**
 * Shared-Memory Inter-Process-Communication
 * Manager - Creates & Removes memory
 */
#ifndef SHMEM_MANAGER_H_
#define SHMEM_MANAGER_H_


#include <cstddef> // size_t
#include <cstdint> // uint8_t
#include <cstring> // strlen, strncpy

#include <stdexcept> // C++ standard exceptions

#include <boost/interprocess/mapped_region.hpp>
#include <boost/interprocess/shared_memory_object.hpp>
#include <boost/interprocess/sync/interprocess_mutex.hpp>


namespace shmem {

class Manager {
	public:
		/**
		 * Constructor
		 * @param name - ID of memory (for Boost library)
		 * @param length - length of data
		 */
		Manager(const char* name, ::std::size_t length){
			// check arguments
			if(name == nullptr){
				throw ::std::invalid_argument("Null-pointer as shared memory ID given!");
			}
			else if(!length){
				throw ::std::invalid_argument("Zero length given!");
			};
			
			// simplify calls
			using namespace ::boost::interprocess;
			
			// create memory
			this->shm = new shared_memory_object(create_only, name, read_write);
			
			// save name for destructor
			::std::size_t len = strlen(name);
			try {
				this->name = new char[len];
				::std::strncpy(this->name, name, len);
				
				// set size to hold the mutex & data
				try {
					this->shm->truncate(sizeof(interprocess_mutex) + length);
					
					// map the whole shared memory to this process to initialize the mutex
					this->region = new mapped_region(*(this->shm), read_write);
					
					// init mutex
					try {
						void* addr = this->region->get_address();
						this->mtx = new (addr) interprocess_mutex;
					}
					catch(...){// catch any exception
						// free region
						delete this->region;
						
						// re-throw catched exception
						throw;
					};
				}
				catch(...){// catch any exception
					// free memory ID
					delete[] this->name;
					
					// re-throw catched exception
					throw;
				};
			}
			catch(...){// catch any exception
				// free shared memory & delete it from system
				delete this->shm;
				shared_memory_object::remove(this->name);
				
				// re-throw catched exception
				throw;
			};
		} // constructor
		
		/**
		 * Destructor
		 */
		~Manager(){
			// no need to free the mutex as it is allocated inside the shared memory
			
			// free region
			delete this->region;
			
			// free shared memory & delete it from system
			delete this->shm;
			::boost::interprocess::shared_memory_object::remove(this->name);
			
			// free memory ID
			delete[] this->name;
		} // destructor
		
		
		/**
		 * Returns base adress of shared memory
		 * @return pointer to base address
		 */
		inline void* getAddress(){
			return (void*) ((::std::uint8_t*) this->region->get_address() + sizeof(*(this->mtx)));
		} // getAddress
		
		/**
		 * Returns size of shared memory
		 * @return size
		 */
		inline ::std::size_t getSize(){
			return this->region->get_size() - sizeof(*(this->mtx));
		} // getSize
		
		/**
		 * Returns associated IPC mutex
		 * @return mutex
		 */
		inline ::boost::interprocess::interprocess_mutex& getMutex(){
			return *(this->mtx);
		} // getMutex
	
	
	private:
		/** ID of memory (for Boost library) */
		char* name;
		
		/** Pointer to Boost shared memory */
		::boost::interprocess::shared_memory_object* shm;
		
		/** Pointer to Boost mapped region of shared memory */
		::boost::interprocess::mapped_region* region;
		
		/** Pointer to Boost mutex */
		::boost::interprocess::interprocess_mutex* mtx;
}; // class Manager

} // namespace shmem


#endif
