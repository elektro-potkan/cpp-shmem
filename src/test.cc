/**
***********************************************
***            C++ Shared Memory            ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***               MIT License               ***
***********************************************
*/


#include <cstdio> // printf

#include "manager.h"


int main(int argc, char **argv){
	// check arguments
	if(argc < 2){
		fprintf(stderr, "Missing shared-memory name argument.\n");
		return -1;
	};
	
	// create memory
	shmem::Manager mem = {argv[1], 150};
	
	printf("Memory created...\n");
	
	// test
	printf("addr=%p\n", mem.getAddress());
	printf("size=%lu\n", mem.getSize());
	
	// test mutex
	mem.getMutex().lock();
	printf("Mutex locked...\n");
	
	mem.getMutex().unlock();
	printf("Mutex unlocked...\n");
	
	return 0;
} // main
