C++ Shared Memory
=================
Headers simplifying managing and using shared memory from Boost library.

Compiling
---------
It is neccessary to provide the needed shared libraries to linker - on Debian `rt` and `pthread`.

Compiler and its parameters used during development:
```bash
g++ -std=c++11 -Wall -Werror -pedantic -lrt -lpthread
```

License
-------
This program is licensed under the MIT License.

See file [LICENSE](LICENSE).
